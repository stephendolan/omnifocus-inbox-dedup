# Deduplication for OmniFocus inbox items

This script searches the inbox for duplicate tasks and removes them.

## Installation

Run `python ./install.py` from this project directory. You can set up the run interval during the process.
