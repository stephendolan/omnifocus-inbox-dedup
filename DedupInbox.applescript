
set taskIterator to 1
set task to getInboxTask(taskIterator)

repeat while task is not equal to "No Task Found"
  set taskName to name of task
  set taskId to id of task
  deleteDuplicatesOf(taskName, taskId)

  set taskIterator to (taskIterator + 1)
  set task to getInboxTask(taskIterator)
end repeat

on deleteDuplicatesOf(taskName, taskId)
  tell application "OmniFocus"
    tell front document
      set theInbox to every inbox task where (completed is not true and dropped is not true)
      if theInbox is not equal to {} then
        repeat with n from 1 to length of theInbox
          set currentTask to item n of theInbox
          set currentName to name of currentTask
          set currentId to id of currentTask

          if currentName is equal to taskName then
            if currentId is not equal to taskId then
              delete currentTask
            end if
          end if
        end repeat
      end if 
    end tell
  end tell
end deleteDuplicateOf

on getInboxTask(indexNumber)
  tell application "OmniFocus"
    tell front document
      set theInbox to every inbox task where (completed is not true and dropped is not true)
      if theInbox is not equal to {} then
        set inboxLength to length of theInbox
        if inboxLength >= indexNumber then
          set myTask to item indexNumber of theInbox
          return myTask
        end if 
      end if 
    end tell
  end tell

  return "No Task Found"
end getInboxTask